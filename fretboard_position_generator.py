from __future__ import  annotations 
from dataclasses import dataclass
from typing import List, Tuple

NUM_FRETS = 24
NUM_STRINGS = 6
# min of abs to get these numbers
BASE_EQUIVALENCE_PATTERN = [-4, -9, -2, -7, 1, -4]
#BASE_EQUIVALENCE_PATTERN = [0, 1, 2, 3, 4, 5]
#BASE_EQUIVALENCE_PATTERN = [0, 6, 0, 6, 0, 6]
#BASE_EQUIVALENCE_PATTERN = [-4, -9, -2, -7, 0, -5]
#BASE_EQUIVALENCE_PATTERN = [0, -5, 2, -3, 4, -1]
FRETBOARD = [["-"] * NUM_FRETS for _ in range(6)]

@dataclass
class FretInfo:
    symbol: str
    color: Tuple[str]


def print_mat_v2(matrix):
    #matrix = [[str(x) for x in range(NUM_FRETS)]] + matrix
    s = [[str(e) for e in row] for row in matrix]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = ' '.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print('\n'.join(table))

def equivalent_notes(n, symbol, color):
    to_be_marked = {}
    #Assuming s = 6
    s, f = n
    for i in range(NUM_STRINGS):
        shift = BASE_EQUIVALENCE_PATTERN[i]
        #to_be_marked.update(eqv_string(NUM_STRINGS-1-i, f + shift, symbol, color))
        to_be_marked.update(eqv_string(i, f + shift, symbol, color))
    return to_be_marked
    
def eqv_string(str_num, start_fret, symbol, color):
    to_be_marked = {}
    # It's possible that start_fret could be invalid, so we have to make it positive (so it exists on the fretboard!)
    while start_fret < 0:
        start_fret += 12
    rem = start_fret % 12
    fret_pos = rem

    while fret_pos <= NUM_FRETS-1:
      to_be_marked[(str_num, fret_pos)] = [FretInfo(symbol, color)]
      fret_pos += 12
    return to_be_marked

def chord_constructor(root, intervals, color):
    to_be_marked = {}
    for i in intervals:
        sym = str(i)
        to_be_marked.update(equivalent_notes((5, root + i), sym, color))
    return to_be_marked


def chord_merge(fret_markings_a, fret_markings_b):

    merged = {}

    for fmx in [fret_markings_a, fret_markings_b]:
        for pos in fmx:
            if pos not in merged:
                merged[pos] = []
            merged[pos].extend(fmx[pos])

    return merged

def chord_remove(chords, rem_chord):
    # Assuming chord is already in there (same memory)
    for pos in rem_chord:
        for fm in rem_chord[pos]:
            chords[pos].remove(fm)

