from fretboard_position_generator import *
import pygame as pg
import random
from math import *


#WIDTH = 1000
#HEIGHT = 1000

FPS = 60

# Define Colors 


## initialize pg and create window
pg.init()
#pg.mixer.init()  ## For sound

WIDTH, HEIGHT = pg.display.Info().current_w, pg.display.Info().current_h

#surface = pg.display.set_mode((WIDTH, HEIGHT))
surface = pg.display.set_mode((WIDTH, HEIGHT), pg.FULLSCREEN | pg.RESIZABLE )
pg.display.set_caption("Fretboard Movement Analyzer")
clock = pg.time.Clock()     ## For syncing the FPS


#function to draw pie using parametric coordinates of circle (takes in radians)
def draw_circle_sector(scr,color,center,radius,start_angle,stop_angle):
    theta=start_angle
    while theta <= stop_angle:
        pg.draw.line(scr,color,center,
        (center[0]+radius*cos(theta),center[1]+radius*sin(theta)),2)
        theta+=0.01

def draw_menu(surface):
    x = WIDTH/3
    y = HEIGHT
    r = pg.Rect((0,0), (x,y))
    pg.draw.rect(surface, pg.Color('white'), r)

class EditableList(pg.sprite.Sprite):
    def __init__(self, x ,y, w, font):
        super().__init__()
        self.x = x 
        self.y = y 
        self.w = w 
        self.font = font
        self.items = []

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def reformat_list(self):
        # This doesn't really work
        # assuming there is at least one item
        # put it back at top
        print("reformatting")
        if len(self.items) != 0:
            self.items[0].rect.x = self.x
            self.items[0].rect.y = self.y

            for i in range(len(self.items)-1):
                li = self.items[i]
                li_y = li.rect.y
                li_height = li.rect.height

                next_li = self.items[i+1]
                next_li.pos[1] = li_y + li_height
                print('rendering', next_li.text)
                next_li.render_text()
                
    

    def add_item(self, text):

        if len(self.items) > 0:
            last_y = self.items[-1].rect.y
            last_height = self.items[-1].rect.height
        else:
            last_y = self.y
            last_height = 0


        data = list(text.split(','))
        str_root, color,  str_intervals = data[0], data[1], data[2::]
        root = int(str_root)
        intervals = [int(x) for x in str_intervals]

        chord = chord_constructor(root, intervals, pg.Color(color))

        fb.fret_markings = chord_merge(fb.fret_markings,chord )

        fb.chord_state.append(chord)

        li = ListItem(self.x, last_y + last_height, self.w, self.font, text, chord, self) 

        self.items.append(li)

        group.add(li)

        #self.items.append(TextInputBox(self.x, last_y + last_height, self.w, self.font, self))
        
    def update(self, event_list):
      for event in event_list:
        for li in self.items:
          if event.type == pg.MOUSEBUTTONDOWN and li.relative_to_whole_page_delete_rect.collidepoint(event.pos):
            li.delete()
            break




class ListItem(pg.sprite.Sprite):
    def __init__(self, x, y, w, font, text, fret_intervals, parent):
        super().__init__()
        self.color = (255, 255, 255)
        self.backcolor = None
        self.pos = [x, y]
        self.width = w
        self.font = font
        self.text = text
        self.fret_intervals = fret_intervals
        self.render_text()
        self.parent = parent

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def delete(self):
        print("deleted called")
        group.remove(self)
        self.parent.items.remove(self)
        chord_remove(fb.fret_markings,self.fret_intervals)

    def render_text(self):
        t_surf = self.font.render(self.text, True, self.color, self.backcolor)
        self.image = pg.Surface((max(self.width, t_surf.get_width()+10), t_surf.get_height()+10), pg.SRCALPHA)
        if self.backcolor:
            self.image.fill(self.backcolor)
        self.image.blit(t_surf, (5, 5))
        pg.draw.rect(self.image, self.color, self.image.get_rect().inflate(-2, -2), 2)
        self.rect = self.image.get_rect(topleft = self.pos)

        corner = ((self.rect.x + self.rect.width + -self.rect.height), 0)
        shifted_corner = ((self.rect.x + self.rect.width + -self.rect.height), self.rect.y)

        # It will be used relative to the textbox around it (see alpha)
        self.delete_rect = pg.Rect(corner, (self.rect.height,self.rect.height))

        # Used to check for mouse clicks
        self.relative_to_whole_page_delete_rect = pg.Rect(shifted_corner, (self.rect.height,self.rect.height))
        print(shifted_corner, (self.rect.height,self.rect.height))

        # (alpha)
        pg.draw.rect(self.image,pg.Color('red'), self.delete_rect)

    def update(self, event_list):
        for event in event_list:
            if event.type == pg.MOUSEBUTTONDOWN and self.relative_to_whole_page_delete_rect.collidepoint(event.pos):
                self.delete()

class TextInputBox(pg.sprite.Sprite):
    def __init__(self, x, y, w, font, EL):
        super().__init__()
        self.color = (255, 255, 255)
        self.backcolor = None
        self.pos = (x, y) 
        self.width = w
        self.font = font
        self.active = False
        self.text = ""
        self.render_text()
        self.EL = EL

    def render_text(self):
        t_surf = self.font.render(self.text, True, self.color, self.backcolor)
        self.image = pg.Surface((max(self.width, t_surf.get_width()+10), t_surf.get_height()+10), pg.SRCALPHA)
        if self.backcolor:
            self.image.fill(self.backcolor)
        self.image.blit(t_surf, (5, 5))
        pg.draw.rect(self.image, self.color, self.image.get_rect().inflate(-2, -2), 2)
        self.rect = self.image.get_rect(topleft = self.pos)


    def update(self, event_list):
        for event in event_list:
            if event.type == pg.MOUSEBUTTONDOWN and not self.active:
                self.active = self.rect.collidepoint(event.pos)
            if event.type == pg.KEYDOWN and self.active:
                if event.key == pg.K_RETURN:
                    self.active = False
                    # add to list and reset text
                    self.EL.add_item(self.text)
                    #self.EL.update()
                    self.text = ''
                elif event.key == pg.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    self.text += event.unicode
                self.render_text()


class Fretboard(pg.sprite.Sprite):
    def __init__(self, x,y ):
        super().__init__()

        self.chord_state = []
        self.pos = (x,y)

        self.x_dimension = (WIDTH/3) + 1
        self.y_dimension = HEIGHT + 1

        self.num_strings = 6
        self.num_frets = 17

        self.string_gaps = self.num_strings -1
        self.fret_gaps = self.num_frets - 1

        self.x_padding_percentage = 10/100
        self.y_padding_percentage = 5/100

        self.x_dim_with_padding = self.x_dimension  - 2 * self.x_dimension * self.x_padding_percentage
        self.y_dim_with_padding = self.y_dimension  - 2 * self.y_dimension * self.y_padding_percentage

        self.x_block_size = self.x_dim_with_padding/self.string_gaps
        self.y_block_size = self.y_dim_with_padding/self.fret_gaps

        self.x_padding = self.x_padding_percentage * self.x_dimension
        self.y_padding = self.y_padding_percentage * self.y_dimension

        self.fret_markings = {}

    def draw_board(self):
        # We have to add another pixel to get the last string
        self.image = pg.Surface((self.x_dimension, self.y_dimension), pg.SRCALPHA)

        string_pattern = [4, 9, 2, 7, 11, 4]

        for x in range(self.num_strings):
            scaled_and_padded = x * self.x_block_size + self.x_padding
            pg.draw.line(self.image, pg.Color("white"), (scaled_and_padded, self.y_padding), (scaled_and_padded , self.y_padding + self.y_dim_with_padding))
            t_surf = font.render(str(string_pattern[x]), True, pg.Color('white'))
            self.image.blit(t_surf, (scaled_and_padded, 1/2 * self.y_padding))

        for y in range(self.num_frets):
            scaled_and_padded = y * self.y_block_size + self.y_padding
            pg.draw.line(self.image, pg.Color("white"), (self.x_padding, scaled_and_padded), (self.x_padding + self.x_dim_with_padding, scaled_and_padded))
            t_surf = font.render(str(y), True, pg.Color('white'))
            self.image.blit(t_surf, (self.x_padding + self.x_dim_with_padding + 1/2 * self.x_padding, scaled_and_padded))

        self.rect = self.image.get_rect(topleft=self.pos)


    def draw_fret_markings(self):
        for pos in self.fret_markings:
            info = self.fret_markings[pos]
            n = len(info)
            for i in range(n):
                scaled_pos = (pos[0] * self.x_block_size + self.x_padding, pos[1] * self.y_block_size + self.y_padding)
                start_angle = (2 * pi)/n * i
                end_angle = (2 * pi)/n * (i+1)
                radius = min(self.x_block_size, self.y_block_size)/2

                draw_circle_sector(self.image, info[i].color, scaled_pos, radius, start_angle, end_angle)
                halfway_angle = start_angle + (2 * pi)/n * 1/2
                halfway_x = cos(halfway_angle) * radius * 3/4
                halfway_y = sin(halfway_angle) * radius * 3/4

                font = pg.font.SysFont(None, ceil(radius/ max(log(n,3),1) ))

                c = info[i].color

                inverse_color = (255 - c[0], 255 - c[1], 255 - c[2])

                t_surf = font.render(info[i].symbol, True, inverse_color)

                t_rect_unmoved = t_surf.get_rect(center=scaled_pos)

                t_rect = t_rect_unmoved.move(halfway_x, halfway_y)

                text_pos_x = scaled_pos[0] + halfway_x
                text_pos_y = scaled_pos[1] + halfway_y

                #self.image.blit(t_surf, (text_pos_x, text_pos_y))
                if (n == 1):
                    self.image.blit(t_surf, t_rect_unmoved)
                else:
                    self.image.blit(t_surf, t_rect)


    def update(self, event_list):
        self.draw_board()
        self.draw_fret_markings()



# Initialize text
font = pg.font.SysFont(None, 20)

editable_list = EditableList(0, 50, WIDTH/3, font)
text_input_box = TextInputBox(0, 0, WIDTH/3, font, editable_list)
fb = Fretboard(WIDTH/3, 0)

group = pg.sprite.Group(text_input_box)
group.add(fb)
group.add(editable_list)


## Game loop
running = True
while running:

    #1 Process input/events
    clock.tick(FPS)     ## will make the loop run at the same speed all the time
    event_list = pg.event.get()
    for event in event_list:        # gets all the events which have occured till now and keeps tab of them.
        ## listening for the the X button at the top
        if event.type == pg.QUIT:
            running = False

    group.update(event_list)



    #3 Draw/render
    surface.fill(pg.Color('black'))

    
    ########################

    ### Your code comes here

    #draw_menu(surface)
    group.draw(surface)


    ########################

    ## Done after drawing everything to the surface
    pg.display.flip()       

pg.quit()



